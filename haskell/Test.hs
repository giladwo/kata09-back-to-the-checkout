#!/usr/bin/env stack
-- stack --resolver lts-16.10 script --package HUnit,transformers,containers

{-# LANGUAGE BangPatterns #-}

module Test where

import Main (Cart, Merchandise(..), addToCart, buyStuff, buyStuffP, checkout, validate)

import Data.Functor.Identity
import Control.Monad.Trans.State.Strict
import Data.Monoid
import Data.Map.Strict
import Test.HUnit

main :: IO ()
main = runTestTT tests >>= print

tests :: Test
tests = test
    [ "price" ~: test
        [ let verify :: Int -> String -> Test
              verify = flip $ (~?=) . buyStuffP

           in "via " ++ title ~: test
            [ "totals" ~: test
                [ "basic" ~: test
                    [ verify 0 ""
                    , verify 50 "A"
                    , verify 80 "AB"
                    , verify 115 "CDBA"
                    ]
                , "repetitions" ~: test
                    [ verify 100 "AA"
                    , verify 130 "AAA"
                    , verify 180 "AAAA"
                    , verify 230 "AAAAA"
                    , verify 260 "AAAAAA"
                    ]
                , "mixed repetitions" ~: test
                    [ verify 160 "AAAB"
                    , verify 175 "AAABB"
                    , verify 190 "AAABBD"
                    , verify 190 "DABABA"
                    ]
                ]
            , "incremental" ~: test
                [ verify 50 "A"
                , verify 80 "AB"
                , verify 130 "ABA"
                , verify 160 "ABAA"
                , verify 175 "ABAAB"
                ]
            ]
      | (title, f) <- [("state monad", buyStuff), ("normal functions", Identity . buyStuffP)]
      ]
    , "validate" ~: test
        (( validate "" @? "Empty cart is allowed"
         -- TODO: use QuickCheck instead
         ) : [ assert $ validate $ replicate n c | n <- [0..3], c <- ['A'..'D']])
    ]

main1 :: IO ()
main1 = print =<< getSum . checkout <$> execStateT action empty
    where
        action :: Monad m => StateT Cart m ()
        action = addToCart A >> addToCart B >> addToCart C >> addToCart B >> addToCart C

    {-
-- TODO: figure out monad transformers in order to implement the incremental tests actually incrementally
main3 :: IO ()
main3 = print =<< getSum . checkout <$> execStateT action empty
    where
        -- printIncremental :: StateT Cart IO ()
        printIncremental = do
            current <- get
            pure $ print $ "Incremental price: " ++ (show $ getSum $ checkout current)

        action :: StateT Cart IO ()
        action = do addToCart A
                    printIncremental
                    addToCart B
                    printIncremental
                    addToCart C
                    printIncremental
                    addToCart B
                    printIncremental
                    addToCart C
                    printIncremental
    --}
