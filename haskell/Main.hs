#!/usr/bin/env stack
-- stack --resolver lts-16.10 script

{-# LANGUAGE BangPatterns #-}

module Main where

import System.Environment.Blank (getArgs)
import Data.List (nub, (\\))
import Data.Functor.Identity
import Control.Monad.Trans.State.Strict
import Data.Foldable (foldl')
import Data.Map.Strict (Map, fromList, foldMapWithKey, empty, alter)
import Data.Monoid

main :: IO ()
main = do
    args <- getArgs
    case args of
        [purchases] | validate purchases ->
            (putStrLn . (("The price of " ++ purchases ++ " is ") ++) . show) =<< buyStuff purchases

        _ -> print "Usage: ./Main (A|B|C|D)*"

-- P stands for Pure (StateT over Identity can easily be replaced with  normal functions)
mainP :: IO ()
mainP = putStrLn . ("The price of ABCBC is " ++) . show . getSum . checkout $ actionP empty
    where
        actionP :: Cart -> Cart
        actionP = addToCartP C . addToCartP B . addToCartP C . addToCartP B . addToCartP A

        -- alternatively: getSum . checkout <$> execStateT action empty
        action :: Monad m => StateT Cart m ()
        action = addToCart A >> addToCart B >> addToCart C >> addToCart B >> addToCart C

type Cart = Map Merchandise (Sum Int)

type Count = Sum Int
type Price = Sum Int

data Merchandise = A | B | C | D deriving (Show, Read, Eq, Ord, Enum, Bounded)

validate :: String -> Bool
validate purchases = null $ nub purchases \\ (concat $ map show ([minBound .. maxBound] :: [Merchandise]))

 {- Prices
    Item    Unit   Special
    Name    Price  Price
     A       50    3 for 130
     B       30    2 for 45
     C       20
     D       15
  -}

pay :: Price -> Merchandise -> Count -> Price
pay !total A !n
    | n >= 3    = pay (total + 130) A (n - 3)
    | otherwise = total + 50 * n
pay !total B !n
    | n >= 2    = pay (total + 45) B (n - 2)
    | otherwise = total + 30 * n
pay !total C !n = total + 20 * n
pay !total D !n = total + 15 * n

checkout :: Cart -> Sum Int
checkout = foldMapWithKey $ pay mempty

buyStuff :: Monad m => String -> m Int
buyStuff = fmap (getSum . checkout) . flip execStateT empty . foldl' (flip $ (>>) . addToCart . read . pure) (pure id)

addToCart :: Monad m => Merchandise -> StateT Cart m ()
addToCart = modify . alter (Just . maybe 1 (+1))

emptyCart :: Monad m => StateT Cart m ()
emptyCart = put empty

addToCartP :: Merchandise -> Cart -> Cart
addToCartP = alter $ Just . maybe 1 (+1)

emptyCartP :: Cart -> Cart
emptyCartP = const empty

buyStuffP :: String -> Int
buyStuffP = getSum . checkout . flip ($) empty . foldl' (flip $ (.) . addToCartP . read . pure) id
